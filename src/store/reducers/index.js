import { combineReducers } from "redux";
import todoReducer from "./todoReducers";
import counterReducer from "./counterReducers";

export default combineReducers({
  todoReducer,
  counterReducer,
});
