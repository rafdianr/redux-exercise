const initialState = {
  counter: 0,
};

const counterReducer = (state = initialState, action) => {
  const { type } = action;
  switch (type) {
    case "UP":
      return {
        ...state,
        counter: state.counter + 1,
      };
    case "DOWN":
      return {
        ...state,
        counter: state.counter - 1,
      };
    default:
      return {
        ...state,
      };
  }
};

export default counterReducer;
