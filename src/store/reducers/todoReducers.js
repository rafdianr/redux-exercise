const initialState = {
  todos: [
    {
      id: 1,
      title: "delectus aut autem",
      completed: false,
    },
    {
      id: 2,
      title: "quis ut nam facilis et officia qui",
      completed: true,
    },
  ],
};

const todoReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case "ADD":
      return {
        ...state,
        todos: [...state.todos, payload],
      };
    case "DEL":
      return {
        ...state,
        todos: state.todos.filter((todo) => todo.id !== payload),
      };
    case "GETBYID":
      const data = state.todos.find((item) => item.id === payload);
      console.log(data);
      return {
        ...state,
      };
    default:
      return {
        ...state,
      };
  }
};

export default todoReducer;
