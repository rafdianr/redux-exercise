import React, { useState } from "react";
import { addTodo } from "../store/actions/todoAction";
import { connect } from "react-redux";

const AddTodo = ({ lists, addTodo }) => {
  const [title, setTitle] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();
    const newTodo = {
      id: lists.length + 1,
      title: title,
      completed: false,
    };
    addTodo(newTodo);
    setTitle("");
  };

  const change = (e) => {
    setTitle(e.target.value);
  };

  return (
    <form action="" onSubmit={onSubmit}>
      <input type="text" value={title} onChange={change} />
      <button>Add</button>
    </form>
  );
};

const mapStateToProps = (state) => ({
  lists: state.todoReducer.todos,
});

export default connect(mapStateToProps, { addTodo })(AddTodo);
