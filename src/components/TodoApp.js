import React, { Component } from "react";
import TodoList from "./TodoList";
import AddTodo from "./AddTodo";

class TodoApp extends Component {
  render() {
    return (
      <div className="App">
        <h1>Todo List</h1>
        <AddTodo />
        <TodoList />
      </div>
    );
  }
}

export default TodoApp;
