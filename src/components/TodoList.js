import React from "react";
import { connect } from "react-redux";
import { delTodo, getTodo } from "../store/actions/todoAction";
import "../assets/style.css";

const TodoList = ({ lists, delTodo, getTodo }) => {
  return (
    <div>
      <h2>List</h2>
      {lists.map((item) => (
        <div key={item.id}>
          <p
            className={item.completed ? "merah" : "biru"}
            onClick={() => getTodo(item.id)}
          >
            {item.title}
          </p>
          <button onClick={() => delTodo(item.id)}>delete</button>
        </div>
      ))}
    </div>
  );
};

const mapStateToProps = (state) => ({
  lists: state.todoReducer.todos,
});

export default connect(mapStateToProps, { delTodo, getTodo })(TodoList);
