import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { countUp, countDown } from "../store/actions/counterAction";

const CounterApp = () => {
  const { counter } = useSelector((state) => state.counterReducer);
  const countFunc = useDispatch();

  return (
    <div className="App">
      <h1>Counter</h1>
      <h2>{counter}</h2>
      <div>
        <button onClick={() => countFunc(countUp())}>up</button>
        <button onClick={() => countFunc(countDown())}>down</button>
      </div>
    </div>
  );
};

export default CounterApp;
