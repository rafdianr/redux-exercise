import React from "react";
import TodoApp from "./components/TodoApp";
import { Provider } from "react-redux";
import store from "./store";
import "./App.css";
import CounterApp from "./components/CounterApp";

function App() {
  return (
    <Provider store={store}>
      <TodoApp />
      <CounterApp />
    </Provider>
  );
}

export default App;
